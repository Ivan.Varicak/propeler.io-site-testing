class ContactPage {
  contactTitle(){
    return cy.get('.entry-title')
  }
  contactEmailAdress(){
    return cy.get('#post-15 > div > p:nth-child(2) > a')
  }
  contactAdress(){
    return cy.get('.entry-content > :nth-child(4)')
  }
}
export default ContactPage