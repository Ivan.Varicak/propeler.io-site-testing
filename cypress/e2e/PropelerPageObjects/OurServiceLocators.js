class OurService{
   enteryTitle(){
    return cy.get('.entry-title')
   }
   whatWeDo(){
    return cy.get('#post-9 > div > div > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-c4c54c6.elementor-widget.elementor-widget-text-editor > div > div')
   }
}
export default OurService;