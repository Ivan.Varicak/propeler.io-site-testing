class AboutUs {
 aboutUsTitle(){
   return cy.get('.entry-title')
 }
 meetTheTeam(){
    return cy.get('h2')
 }
 teamPics(){
    return cy.get('#post-13 > div > div')
 }
}
export default AboutUs