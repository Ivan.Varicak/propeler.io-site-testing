class ButtonElements {
    
    homePageLogo(){
        return cy.get('.custom-logo')
    }
    homePageButton(){
        return cy.get('#menu-item-123 > a')
    }
    ourServiceButton(){
        return cy.get('#menu-item-20 > a')
    }
    projectsButton(){
        return cy.get('#menu-item-19 > a')
    }
    coursesButton(){
        return cy.get('#menu-item-302 > a')
    }
    aboutUsButton(){
        return cy.get('#menu-item-18 > a')
    }
    contactButton(){
        return cy.get('#menu-item-17 > a')
    }
    serachButton(){
        return cy.get('.search')
    }
    searchInputText(){
        return cy.get('#s')
    }
    searchButtonSubmit(){
        return cy.get('.searchsubmit')
    }

}
export default ButtonElements;