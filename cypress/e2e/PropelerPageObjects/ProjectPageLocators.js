class ProjectPageElements {
    projectsPageTitle(){
        return cy.get('.entry-title')
    }
    projectsTitle() {
        return cy.get('.elementor-element-437ac6b > .elementor-widget-container > .elementor-text-editor > h3')
    }
    firstProjectVideo() {
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > section.elementor-section.elementor-inner-section.elementor-element.elementor-element-481c773.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default > div > div > div.elementor-column.elementor-col-50.elementor-inner-column.elementor-element.elementor-element-9bb1d37 > div > div > div > div > div > a')
    }
    projectsFirstTitle() {
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-9add2ee.elementor-widget.elementor-widget-heading > div > h4')
    }
    projectsFirstPic() {
        return cy.get('#gallery-1')
    }
    projectSecondTitle() {
        return cy.get('.elementor-element-ab998fe > .elementor-widget-container > .elementor-text-editor > h3')
    }
    projectSecondPic() {
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-2dc7161.elementor-widget.elementor-widget-image > div > div')
    }
    projectsThirdTitle(){
        return cy.get('.elementor-element-486fe73 > .elementor-widget-container > .elementor-text-editor > h3')
    }
    secondProjectsVideo(){
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > section.elementor-section.elementor-inner-section.elementor-element.elementor-element-b9a1f5f.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default > div > div > div.elementor-column.elementor-col-66.elementor-inner-column.elementor-element.elementor-element-cb85ddf > div > div > div > div > div > a')
    }
    thirdProjectTitle(){
        return cy.get('.elementor-element-d12b1ba > .elementor-widget-container > .elementor-text-editor > h3')
    }
    projectThirdPic(){
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-e4135b3.elementor-widget.elementor-widget-image > div > div')
    }
    forthProjectsTitle(){
        return cy.get('.elementor-element-92d5b86 > .elementor-widget-container > .elementor-text-editor > h3')
    }
    forthProjectsPic(){
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > section.elementor-section.elementor-inner-section.elementor-element.elementor-element-43f7164.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default > div > div > div.elementor-column.elementor-col-33.elementor-inner-column.elementor-element.elementor-element-7288fc7 > div > div > div > div > div')
    }
    projectText(){
        return cy.get('#post-11 > div > div > div > div > section > div > div > div > div > div > section.elementor-section.elementor-inner-section.elementor-element.elementor-element-43f7164.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default > div > div > div.elementor-column.elementor-col-66.elementor-inner-column.elementor-element.elementor-element-78d6a49 > div > div > div > div > div')
    }
    projectGalerry(){
        return  cy.get('#gallery-5')
    }


}
export default ProjectPageElements