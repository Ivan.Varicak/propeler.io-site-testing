class Courses {
 coursesButtonTitle(){
    return cy.get('.entry-title')
 }
 secondTitle(){
   return cy.get('#post-293 > div > h3 > strong')
 }
 coursesTutor(){
    return cy.get('.entry-content > :nth-child(3) > :nth-child(1)')
 }
 coursesTutorEmail(){
    return cy.get('#post-293 > div > h4:nth-child(3) > a')
 }
 coursesLearning(){
    return cy.get(':nth-child(8) > strong')
 }
 coursesRequirements(){
    return cy.get(':nth-child(10) > strong')
 }
 coursesFor(){
    return cy.get(':nth-child(12) > strong')
 }
 coursesTopics(){
     return cy.get(':nth-child(14) > strong')
 }
 coursesSettings(){
    return cy.get(':nth-child(16) > strong')
 }
 
}
export default Courses;