class HomePage {
    homePageTitle(){
        return cy.get('.entry-title')
    }
    readMore(){
        return cy.get(':nth-child(1) > .btn')
    }
    yourName(){
        return cy.get('#textbox5')
    }
    yourEmail(){
        return cy.get('#email6')
    }
    textArea(){
        return cy.get('#textarea7')
    }
    submitButton(){
        return cy.get('#submitButton4')
    }
    emailAdress(){
        return cy.get('.col-md-4 > :nth-child(2) > a')
    }

}

export default HomePage;