/// <reference types="cypress"/>

import AboutUs from "../../e2e/PropelerPageObjects/AboutUsLocators"
import ButtonElements from "../../e2e/PropelerPageObjects/ButtonLocators"
import ContactPage from "../../e2e/PropelerPageObjects/ContactPageLocators"
import Courses from "../../e2e/PropelerPageObjects/CoursesLocators"
import HomePage from "../../e2e/PropelerPageObjects/HomePageLocators"
import OurService from "../../e2e/PropelerPageObjects/OurServiceLocators"
import ProjectPageElements from "../../e2e/PropelerPageObjects/ProjectPageLocators"


const buttonElements = new ButtonElements
const homePageElements = new HomePage
const ourSericeElements = new OurService
const projectPageElements = new ProjectPageElements
const coursesPageElements = new Courses
const aboutUsPageElements = new AboutUs
const contactPageElements = new ContactPage
let data;

beforeEach(function () {

  cy.fixture('propeler').then(function (propelerData) {
      data = propelerData
  })
})


describe('Testing Proeler io', () => {
 
  beforeEach(() => {
    cy.visit('https://propeler.io/')
  })

  it('Testing url', () => {
    cy.url().should('eq', 'https://propeler.io/')
  })
  it('Testing Home page Logo button', () => {
    buttonElements.homePageLogo().should('have.attr', 'src')
    buttonElements.homePageLogo().click()
    cy.url().should('eq', 'https://propeler.io/')
  })
  it('Testing Home button', () => {
    buttonElements.homePageButton().click()
    cy.url().should('eq', 'https://propeler.io/')
  })
  it('Testing Our Service button', () => {
    buttonElements.ourServiceButton().click()
    cy.url().should('eq', 'https://propeler.io/our-services/')
    ourSericeElements.enteryTitle().should('have.text', 'Our Services')
  })
  it('Testing Projects button', () => {
    buttonElements.projectsButton().click()
    cy.url().should('eq', 'https://propeler.io/projekti/')
    projectPageElements.projectsPageTitle().should('have.text', 'Projects')
  })
  it('Testing Courses button', () => {
    buttonElements.coursesButton().click()
    cy.url().should('eq', 'https://propeler.io/courses/')
    coursesPageElements.coursesButtonTitle().should('have.text', 'Courses')
  })
  it('Testing About Us button', () => {
    buttonElements.aboutUsButton().click()
    cy.url().should('eq', 'https://propeler.io/o-nama/')
    aboutUsPageElements.aboutUsTitle().should('have.text', 'About Us')
  })
  it('Testing Contact Button', () => {
    buttonElements.contactButton().click()
    cy.url().should('eq', 'https://propeler.io/kontakt/')
    contactPageElements.contactTitle().should('have.text', 'Contact')
  })
  it('Testing Serach button', () => {
    buttonElements.serachButton().invoke('show').click()
    buttonElements.searchInputText().type('About us', { force: true })
    buttonElements.searchButtonSubmit().click({ force: true })
    cy.url().should('eq', 'https://propeler.io/?s=About+us')
  })

})

describe('Testing Home page', () => {
  beforeEach(() => {
    cy.visit('https://propeler.io/')
    
    
  })
  it('Testing Home page read more button', () => {
    homePageElements.readMore().click()
    cy.url().should('eq', 'https://propeler.io/projekti/')
    homePageElements.homePageTitle().should('have.text', 'Projects')
    cy.go('back')

  })
  it('Testing Contact on Home page', () => {
    homePageElements.yourName().type(data.name)
    homePageElements.yourEmail().type(data.email)
    homePageElements.textArea().type(data.textArea)
    homePageElements.submitButton().should('have.value', 'SEND')
    homePageElements.emailAdress().should('have.attr', 'href')
  })
})

describe('Testing Our Service page', () => {
  it('Testing "What we do?"', () => {
    cy.visit('https://propeler.io/our-services/')
    cy.url().should('eq', 'https://propeler.io/our-services/')
    ourSericeElements.enteryTitle().should('have.text', 'Our Services')
    ourSericeElements.whatWeDo().should('have.class', 'elementor-text-editor elementor-clearfix')
  })
})

describe('Testing Projects page', () => {

  beforeEach(() => {
    cy.visit("https://propeler.io/projekti/")
  })
  it('Testing "Detect and measure the volume of furniture" part', () => {
    projectPageElements.projectsTitle().should('have.text', 'Projects – Detect and measure the volume of furniture')
    projectPageElements.firstProjectVideo().should('have.attr', 'href')
    projectPageElements.firstProjectVideo().should('have.attr', 'target')
  })
  it('Testing "Technologies" part', () => {
    projectPageElements.projectsFirstTitle().should('have.class', 'elementor-heading-title elementor-size-default')
    projectPageElements.projectsFirstPic().should('have.id', 'gallery-1')
  })
  it('Testing "Self service marketing management platform" part', () => {

    projectPageElements.projectSecondTitle().should('have.text', 'Self service marketing management platform')
    projectPageElements.projectSecondPic().should('have.class', 'elementor-image')
  })
  it('Testing "Detecting problems on streets" part', () => {
    projectPageElements.projectsThirdTitle().should('have.text', 'Detecting problems on streets')
    projectPageElements.secondProjectsVideo().should('have.attr', 'href')
    projectPageElements.secondProjectsVideo().should('have.attr', 'target')
  })
  it('Testing "Counting chips in stack" part', () => {
    projectPageElements.thirdProjectTitle().should('have.text', 'Counting chips in stack')
    projectPageElements.projectThirdPic().should('have.class', 'elementor-image')
  })
  it('Testing "Optimization in Logistics" part', () => {
    projectPageElements.forthProjectsTitle().should('have.text', 'Optimization in Logistics')
    projectPageElements.forthProjectsPic().should('have.class', 'elementor-image')
    projectPageElements.projectText().should('have.class', 'elementor-text-editor elementor-clearfix')
    projectPageElements.projectGalerry().should('have.class', 'gallery galleryid-11 gallery-columns-3 gallery-size-full')
  })
})

describe('Testing Courses page', () => {
  it('Testing Courses page elements', () => {
    cy.visit('https://propeler.io/courses/')
    coursesPageElements.coursesButtonTitle().should('have.text', 'Courses')
    coursesPageElements.secondTitle().should('have.text', 'Data science course – Beginner')
    coursesPageElements.coursesTutor().should('have.text', 'Tutor: ')
    coursesPageElements.coursesTutorEmail().should('have.attr', 'href')
    coursesPageElements.coursesLearning().should('have.text', 'What you’ll learn')
    coursesPageElements.coursesRequirements().should('have.text', 'Are there any course requirements or prerequisites?')
    coursesPageElements.coursesFor().should('have.text', 'Who this course is for:')
    coursesPageElements.coursesTopics().should('have.text', 'What topics will be covered:')
    coursesPageElements.coursesSettings().should('have.text', 'Setting:')
  })
})

describe('Testing About Us page', () => {
  it('Testing About Us page elements', () => {
    cy.visit('https://propeler.io/o-nama/')
    aboutUsPageElements.aboutUsTitle().should('have.text', 'About Us')
    aboutUsPageElements.meetTheTeam().should('have.text', 'Meet the team')
    aboutUsPageElements.teamPics().should('have.class', 'is-layout-flex wp-container-4 wp-block-columns')

  })
})

describe('Testing Contact page', () => {
  it('Testing Cotact page elements', () => {
    cy.visit('https://propeler.io/kontakt/')
    contactPageElements.contactTitle().should('have.text', 'Contact')
    contactPageElements.contactEmailAdress().should('have.attr', 'href')
    contactPageElements.contactAdress().should('have.text', 'Belgrade 11000, Svetogorska 15')
    homePageElements.yourName().type(data.name)
    homePageElements.yourEmail().type(data.email)
    homePageElements.textArea().type(data.textArea)
    homePageElements.submitButton().should('have.value', 'SEND')
  })

})
